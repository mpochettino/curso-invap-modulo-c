/* ---- Weather Data Center ----- */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <vector>

using namespace std;

class Observer
{
public:
    virtual void update(float temperature, float pressure, float humidity) = 0;
};

class Observable
{
protected:
    vector<Observer *> observers;

public:
    virtual void addObserver(Observer *observer) = 0;
    virtual void notifyObservers() = 0;
};

class WeatherData : public Observable
{
private:
    float temperature;
    float pressure;
    float humidity;

public:
    virtual void addObserver(Observer *observer)
    {
        observers.push_back(observer);
    }
    virtual void notifyObservers()
    {
        for (auto observer : observers)
            observer->update(temperature, pressure, humidity);
    }

    float getTemperature()
    {
        return temperature;
    }

    float getHumidity()
    {
        return humidity;
    }

    float getPressure()
    {
        return pressure;
    }

    void getMeasurements()
    {
        cout << "Temperature [C]: ";
        cin >> temperature;
        cout << "Humidity [%]: ";
        cin >> humidity;
        cout << "Pressure [kPa]: ";
        cin >> pressure;
        cout << "\n";
        notifyObservers();
    }
};

class LatestDisplay : public Observer
{
public:
    void update(float temperature, float pressure, float humidity)
    {
        cout << "Current temperature: " << temperature << "C degrees and " << humidity << "% humidity." << endl;
    }
};

class MeanDisplay : public Observer
{
private:
    vector<float> histTemperature;
    float maxTemperature()
    {
        float max = histTemperature[0];

        for (size_t i = 0; i < histTemperature.size(); i++)
        {
            if (histTemperature[i] > max)
                max = histTemperature[i];
        }
        return max;
    }
    float minTemperature()
    {
        float min = histTemperature[0];

        for (size_t i = 0; i < histTemperature.size(); i++)
        {
            if (histTemperature[i] < min)
                min = histTemperature[i];
        }
        return min;
    }
    float meanTemperature()
    {
        float sum = 0;
        int n = histTemperature.size();
        for (size_t i = 0; i < n; i++)
        {
            sum += histTemperature[i] / n;
        }
        return sum;
    }

public:
    void update(float temperature, float pressure, float humidity)
    {
        histTemperature.push_back(temperature);
        cout << "Avg/Max/Min temperature: " << meanTemperature() << "/" << maxTemperature() << "/" << minTemperature() << endl;
    }
};

class ForecastDisplay : public Observer
{
private:
    float lastTemperature;

public:
    ForecastDisplay() : lastTemperature(20) {}
    void update(float temperature, float pressure, float humidity)
    {
        if (lastTemperature > 30 && temperature > lastTemperature && humidity > 80)
        {
            cout << "Forecast: strong storms."<<endl;
        }
        else if (lastTemperature < 2 && temperature < lastTemperature)
        {
            cout << "Forecast: probability of snow."<<endl;
        }
        else if ((lastTemperature < 20 && temperature > lastTemperature) || (lastTemperature > 30 && temperature > lastTemperature))
        {
            cout << "Forecast: improving weather on the way."<<endl;
        }
        else if (lastTemperature < 15 && temperature < lastTemperature)
        {
            cout << "Forecast: a polar wave is expected."<<endl;
        }
        else{
            cout << "Forecast: unknown."<<endl;
        }
        cout <<"\n"<<endl;
        lastTemperature = temperature;
    }
};

int main(int argc, char const *argv[])
{
    WeatherData dataCenter;
    LatestDisplay latestDisplay;
    MeanDisplay meanDisplay;
    ForecastDisplay forecastDisplay;
    dataCenter.addObserver(&latestDisplay);
    dataCenter.addObserver(&meanDisplay);
    dataCenter.addObserver(&forecastDisplay);
    cout << "------ Weather Data Center ------" << endl;
    while (true)
    {
        dataCenter.getMeasurements();
    }

    return 0;
}