#include <stdio.h>
#include <string.h>

int i = 102;
int j = -56;
long ix = -158693157400;
unsigned u = 35460;
float x = 12.687;
double dx = 0.000000025;
char c = 'C';


int main(int argc, char** argv) {

/*  A. Escribir los valores de i, j, x y dx suponiendo que cada cantidad entera tiene una longitud de campo mínima de 
    cuatro caracteres y cada cantidad en coma flotante se presenta en notación exponencial con un total de al menos 
    14 caracteres y no más de 8 cifras decimales.
*/
    printf("A: ");
    printf ("%04i, %04i, %.14F, %.8lf\n\n", i, j, x, dx); 

//  B. Repetir A, visualizando cada cantidad en una línea.
    printf("B: \n");
    printf ("%04i\n%04i\n%.14F\n%.8lf\n\n", i, j, x, dx); 

/*  C. Escribir los valores de i, ix, j, x y u suponiendo que cada cantidad entera tendrá una longitud de campo mínima de cinco caracteres,
    el entero largo tendrá una longitud de campo mínima de 12 caracteres y la cantidad en coma flotante tiene al menos 10 caracteres 
    con un máximo de cinco cifras decimales. No incluir el exponente.
*/
    printf("C: ");
    printf("%05i, %012li, %05i, %.10f, %05u\n\n", i, ix, j, x, u);

/*D. Repetir C, visualizando las tres primeras cantidades en una línea, seguidas de una línea en blanco y las otras dos cantidades
 en la línea siguiente.*/
    printf("D: \n");
    printf("%05i, %012li, %05i\n\n%.10f, %05u\n\n", i, ix, j, x, u);
    

/* E. Escribir los valores de i, u y c, con una longitud de campo mínima de seis caracteres para cada cantidad entera. 
Separar con tres espacios en blanco cada cantidad. */
    printf("E: ");
    printf("%06i, %06u, %6c\n\n", i, u, c);

/*  F. Escribir los valores de j, u y x. Visualizar las tres cantidades enteras con una longitud de campo mínima de cinco caracteres.
    Presentar la cantidad en coma flotante utilizando la conversión tipo f, con una longitud mínima de 11 caracteres y un máximo de 
    cuatro cifras decimales.*/
    printf("F: ");
    printf("%05i, %05u, %11.4f\n\n", j, u, x);

//G. Repetir F, con cada dato ajustado a la izquierda dentro de su campo.
    printf("G: \n");
    printf("|%-5i| \n|%-5u| \n|%-11.4f| \n\n", j, u, x);
    
//H. Repetir F, apareciendo un signo (tanto + como -) delante de cada dato con signo.
    printf("H: ");
    printf("%05i, +%05u, +%11.4f\n\n", j, u, x);

//I. Repetir F, rellenando el campo de cada entidad entera con ceros.
    printf("I: ");
    printf("%05i, %05u, %011.4f\n\n", j, u, x);

//J. Repetir F, apareciendo el valor de x con un punto decimal.
    printf("J: ");
    printf("%05i, %05u, %11.1f\n\n", j, u, x);
    
    return 0;
}
