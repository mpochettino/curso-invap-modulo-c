#include <stdio.h>
#include <string.h>
#include <math.h>

int main(void) {
    int A = 0, n = 0, i = 0, F = 0;
    printf("Cantidad de dinero depositada en cuenta de ahorros: ");
    scanf("%i", &A);
    printf("Cantidad de años: ");
    scanf("%i", &n);
    printf("Interés anual: ");
    scanf("%i", &i);
    
    double resultado;
    for (int x=1; x < n; x++){
        double resultadoActual = pow((1.+i*1.0/100),1.0*n);
        resultado = resultado + resultadoActual;
        printf("%f\n", (pow(1.+i*1.0/100,1.0*n)));
        printf("%lf\n", resultado);
    }
    F = A * resultado;

    printf("La cantidad de dinero que se acumulara si se depositan %i dólares tras %i años con un interes de %i es: %i\n", A, n, i, F);

    int dinero;
    dinero = 100000 / resultado;

    printf("Si se quiere acumular 100.000 dólares en %i años se deben depositar: %i dólares. \n", n, dinero);
    return 0;


}