#include <stdio.h>
#include <string.h>


int main(void) {
    char texto[80] = {0};
    printf("Escribir un texto de máximo 80 caracteres: \n");
    scanf("%[^\n]", texto);
    char inverso[80] = {0};
    int len_texto = strlen(texto);
    for(int i= 0; i < len_texto; i++){
        inverso[len_texto -1 -i] = texto[i];
    }
    printf("El texto escrito de forma inversa es el siguiente: \n");
    printf("%s\n\n", inverso);

    return 0;


}