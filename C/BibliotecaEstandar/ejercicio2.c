/* Escriba un programa que sea capaz de contar las palabras en un archivo de texto plano
mediante el uso de funciones de entrada/salida de archivos (file.h)
y de la biblioteca de manejo de cadenas de caracteres (string.h).
Compruebe el funcionamiento con un archivo trivial de dos palabras (Hola Mundo!) y luego
utilice el archivo lorem_ipsum.txt suministrado por la materia.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


struct DetalleDePalabra
{
    char palabra[100];
    int frecuencia;
};

struct nodo
{
    struct DetalleDePalabra detalleDePalabra;
    struct nodo *siguiente;
};

struct nodo *superior = NULL;

void agregar(struct DetalleDePalabra detalleDePalabra)
{

    struct nodo *nuevoNodo = malloc(sizeof(struct nodo));
    nuevoNodo->detalleDePalabra = detalleDePalabra;
    nuevoNodo->siguiente = superior;
    superior = nuevoNodo;
}

void agregarPalabra(char palabra[100])
{
    struct nodo *temp = superior;
    while (temp != NULL)
    {
        //Comprobar si existe la palabra:
        int resultadoDeComparacion =
            strcasecmp(temp->detalleDePalabra.palabra, palabra);
        if (resultadoDeComparacion == 0)
        {
            temp->detalleDePalabra.frecuencia++;
            return;
        }
        temp = temp->siguiente;
    }
    //Sino, agregar nueva:
    struct DetalleDePalabra detalleDePalabra;
    strcpy(detalleDePalabra.palabra, palabra);
    detalleDePalabra.frecuencia = 1;
    agregar(detalleDePalabra);
}

void imprimir(void)
{

    //Se crea la tabla:
    char guiones[] = "--------------------";
    printf("+%s+%s+\n", guiones, guiones);
    printf("|%-20s|%-20s|\n", "PALABRA", "FRECUENCIA");
    printf("+%s+%s+\n", guiones, guiones);

    struct nodo *temp = superior;
    while (temp != NULL)
    {
        printf("|%-20s|%-20d|\n", temp->detalleDePalabra.palabra,
               temp->detalleDePalabra.frecuencia);
        temp = temp->siguiente;
    }
    printf("+%s+%s+\n", guiones, guiones);
}

int main(int argc, char **argv)
{
    FILE *f;
    int i;
    char texto[200];

    if (argc < 2)
    {
        //Ingresar texto:
        printf("Ingresa un texto de 200 caracteres. Luego presiona enter.\n");
        fgets(texto, 200, stdin);
        printf("\n\n");
    }
    else
    {
        f = fopen(argv[1], "r");
        if (f == NULL)
        {
            fprintf(stderr, "No se pudo abrir el archivo %s\n", argv[1]);
            exit(EXIT_FAILURE);
        }
        int i = 0;
        char palabra[100] = {0};
        int len;
        int pos = 0;
        while (!feof(f))
        {
            fscanf(f, "%s", palabra);
            len = strlen(palabra);
            for (size_t i = 0; i < strlen(palabra); i++)
            {
                texto[pos + i] = tolower(palabra[i]);
            }
            texto[pos + len] = ' ';
            pos += len + 1;
        }
        printf("%s", texto);
        printf("\n\n");
        fclose(f);
    }

    //Delimitar caracteres especiales:
    char delimitador[] = ",;:. \n!\"'";

    //Sacar el primer token:
    char *token = strtok(texto, delimitador);

    //Sacar los siguientes y agregar la palabra a la pila:
    while (token != NULL)
    {
        agregarPalabra(token);
        token = strtok(NULL, delimitador);
    }

    printf("La frecuencia de las palabras del texto es la siguiente: \n");
    imprimir();

    exit(EXIT_SUCCESS);
}