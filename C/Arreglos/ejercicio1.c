#include <string.h>
#include <stdio.h>
#include <math.h>

int main()
{
    float array[10] = {4.7, -8.0, -2.3, 11.4, 12.9, 5.1, 8.8, -0.2, 6.0, -14.7,};
    int i = 0;
    int j = 0;
    int temp = 0;
    int x = 0;
    int len = 10;
    double arrayAbsoluto[10];

    // Ordenamiento de menor a mayor en valor absoluto.
    for (i = 0; i < len; i++)
    {
        if (array[i] < 0)
        {
            arrayAbsoluto[i] = array[i] * -1;
        }
        else
        {
            arrayAbsoluto[i] = array[i];
        }
    }

    for (i = 1; i < len; i++)
    {
        for (j = 0; j < len - 1; j++)
        {
            if (arrayAbsoluto[j] > arrayAbsoluto[j + 1])
            {
                x = arrayAbsoluto[j];
                arrayAbsoluto[j] = arrayAbsoluto[j + 1];
                arrayAbsoluto[j + 1] = x;
            }
        }
    }
    printf("Array ordenado de menor a mayor en valor absoluto: \n");
    for (size_t i = 0; i < len; i++)
    {
        printf("%lf ", arrayAbsoluto[i]);
    }
    printf("\n\n");

    // Ordenamiento de menor a mayor algebraicamente (con signo).
    for (i = 1; i < len; i++)
    {
        for (j = 0; j < len - 1; j++)
        {
            if (array[j] > array[j + 1])
            {
                x = array[j];
                array[j] = array[j + 1];
                array[j + 1] = x;
            }
        }
    }
    printf("Array ordenado de menor a mayor algebraicamente: \n");
    for (size_t i = 0; i < len; i++)
    {
        printf("%f ", array[i]);
    }
    printf("\n\n");

    // Ordenamiento de mayor a menor en valor absoluto.
    for (i = 1; i < len; i++)
    {
        for (j = 0; j < len - 1; j++)
        {
            if (arrayAbsoluto[j] < arrayAbsoluto[j + 1])
            {
                x = arrayAbsoluto[j];
                arrayAbsoluto[j] = arrayAbsoluto[j + 1];
                arrayAbsoluto[j + 1] = x;
            }
        }
    }
    printf("Array ordenado de mayor a menor en valor absoluto: \n");
    for (size_t i = 0; i < len; i++)
    {
        printf("%lf ", arrayAbsoluto[i]);
    }
    printf("\n\n");

    // Ordenamiento de mayor a menor algebraicamente (con signo).
    for (i = 1; i < len; i++)
    {
        for (j = 0; j < len - 1; j++)
        {
            if (array[j] < array[j + 1])
            {
                x = array[j];
                array[j] = array[j + 1];
                array[j + 1] = x;
            }
        }
    }
    printf("Array ordenado de mayor a menor algebraicamente: \n");
    for (size_t i = 0; i < len; i++)
    {
        printf("%f ", array[i]);
    }
    printf("\n\n");

    return 0;
};
