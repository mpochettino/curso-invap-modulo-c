#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// Definición de variables y funciones:
enum Palo
{
    PICA,
    DIAMANTE,
    TREBOL,
    CORAZON
};

typedef struct Carta_t
{
    int valor;
    enum Palo palo;
} Carta;

// Verificar si la carta ya salió:
int comparaCartas(Carta carta1, Carta carta2)
{
    if (carta1.valor == carta2.valor && carta1.palo == carta2.palo)
        return 1;
    return 0;
}

// Mezclar y obtener una carta del mazo:
Carta obtieneCarta(Carta *cartasJugador1, Carta *cartasJugador2, int lenCartasJugador1, int lenCartasJugador2)
{
    Carta carta;
    int yaSalio = 0;
    do
    {
        yaSalio = 0;
        carta.valor = (rand() % 13) + 1;
        carta.palo = rand() % 4;
        for (size_t i = 0; i < lenCartasJugador1; i++)
        {
            if (comparaCartas(carta, cartasJugador1[i]) == 1)
                yaSalio = 1;
        }
        for (size_t i = 0; i < lenCartasJugador2; i++)
        {
            if (comparaCartas(carta, cartasJugador2[i]) == 1)
                yaSalio = 1;
        }
    } while (yaSalio);
    return carta;
}

// Imprimir qué carta le tocó a cada jugador/a:
void imprimeCarta(Carta carta)
{
    switch (carta.palo)
    {
    case PICA:
        printf("%i de ♠. ", carta.valor);
        break;
    case TREBOL:
        printf("%i de ♣. ", carta.valor);
        break;
    case DIAMANTE:
        printf("%i de ♦. ", carta.valor);
        break;
    case CORAZON:
        printf("%i de ♥. ", carta.valor);
        break;

    default:
        printf("Error");
        break;
    }
}

// Calcular el puntaje:
int calculaSuma(Carta *cartas, int cantCartas)
{
    int suma = 0;
    int valor;
    int numAses = 0;
    for (size_t i = 0; i < cantCartas; i++)
    {
        valor = cartas[i].valor;
        if (valor == 11 || valor == 12 || valor == 13)
            valor = 10;
        if (valor == 1)
        {
            valor = 11;
            numAses++;
        }
        suma += valor;
    }
    while (suma > 21 && numAses > 0)
        suma -= 10;
    return suma;
}

// Comparar los puntajes y encontrar a la persona ganadora:
void encuentraGanador(Carta *cartasJugador1, Carta *cartasJugador2, int lenCartasJugador1, int lenCartasJugador2)
{
    int sumaJugador1 = calculaSuma(cartasJugador1, lenCartasJugador1);
    int sumaJugador2 = calculaSuma(cartasJugador2, lenCartasJugador2);
    printf("Jugadora 1: %d puntos, Jugador 2: %d puntos.\n", sumaJugador1, sumaJugador2);

    if (sumaJugador1 > 21)
    {
        if (sumaJugador2 <= 21)
        {
            printf("Gana jugador 2.\n");
            return;
        }
        printf("Ambos pierden.\n");
        return;
    }
    if (sumaJugador1 <= 21)
    {
        if (sumaJugador2 > 21 || sumaJugador1 > sumaJugador2)
        {
            printf("Gana jugadora 1.\n");
            return;
        }
        if (sumaJugador2 > sumaJugador1)
        {
            printf("Gana jugador 2.\n");
            return;
        }
    }
    printf("Empate.\n");
    return;
}

// BLACKJACK 
int main(void)
{
    int valorCarta, inputJugador1, inputJugador2, lenCartasJugador1, lenCartasJugador2, sigueJugando1, sigueJugando2;
    Carta cartasJugador1[8], cartasJugador2[8], carta;
    inputJugador1 = 0;
    inputJugador2 = 0;
    lenCartasJugador1 = 0;
    lenCartasJugador2 = 0;
    sigueJugando1 = 1;
    sigueJugando2 = 1;
    enum Palo palo;
    srand(time(NULL));
    printf("# ---------------- BIENVENIDXS AL BLACKJACK ---------------- # \n\n");

    cartasJugador1[0] = obtieneCarta(cartasJugador1, cartasJugador2, lenCartasJugador1, lenCartasJugador2);
    lenCartasJugador1++;
    cartasJugador2[0] = obtieneCarta(cartasJugador1, cartasJugador2, lenCartasJugador1, lenCartasJugador2);
    lenCartasJugador2++;

    printf("El objeto del juego es obtener 21 puntos, o tantos puntos como sea posible sin exceder de 21 en cada mano.\n Las figuras cuentan 10 puntos y un as puede contar como 1 u 11 puntos.\n\n");


    printf("Carta inicial para jugadora 1: \n");
    imprimeCarta(cartasJugador1[0]);
    printf("\n\n");
    printf("Carta inicial para jugador 2: \n");
    imprimeCarta(cartasJugador2[0]);
    printf("\n\n");

    while (calculaSuma(cartasJugador1, lenCartasJugador1) <= 21 &&
           calculaSuma(cartasJugador2, lenCartasJugador2) <= 21 &&
           (sigueJugando1 || sigueJugando2))
    {
        // # ------------------- JUGADORA 1 ------------------------- #
        printf("Jugadora 1, por ahora tiene las siguientes cartas: \n");
        for (size_t i = 0; i < lenCartasJugador1; i++)
            imprimeCarta(cartasJugador1[i]);
        printf("Que suman: %d\n", calculaSuma(cartasJugador1, lenCartasJugador1));
        printf("Desea recibir otra carta? (Sí: Ingrese un número mayor a cero. No: 0 (cero).): ");
        scanf("%d", &sigueJugando1);
        if (sigueJugando1)
        {
            carta = obtieneCarta(cartasJugador1, cartasJugador2, lenCartasJugador1, lenCartasJugador2);
            imprimeCarta(carta);
            cartasJugador1[lenCartasJugador1] = carta;
            lenCartasJugador1++;
        }
        printf("Ahora suman: %d\n\n", calculaSuma(cartasJugador1, lenCartasJugador1));

        // # ------------------- JUGADOR 2 ------------------------- #
        printf("Jugador 2, por ahora tiene las siguientes cartas: \n");
        for (size_t i = 0; i < lenCartasJugador2; i++)
            imprimeCarta(cartasJugador2[i]);
        printf("Que suman: %d\n", calculaSuma(cartasJugador2, lenCartasJugador2));
        printf("Desea recibir otra carta? (Sí: Ingrese un número mayor a cero. No: 0 (cero).): ");
        scanf("%d", &sigueJugando2);
        if (sigueJugando2)
        {
            carta = obtieneCarta(cartasJugador1, cartasJugador2, lenCartasJugador1, lenCartasJugador2);
            imprimeCarta(carta);
            cartasJugador2[lenCartasJugador2] = carta;
            lenCartasJugador2++;
        }
        printf("Ahora suman: %d\n\n", calculaSuma(cartasJugador2, lenCartasJugador2));
    }

    encuentraGanador(cartasJugador1, cartasJugador2, lenCartasJugador1, lenCartasJugador2);
    return 0;
}
