/* Equipos de la liga de fútbol */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Equipo_t
{
    char nombre[50];
    int victorias;
    int derrotas;
    int empates;
    int goles;
} Equipo;

void ordenVictorias(int cantidadEquipos, Equipo* equipos);
void ordenDerrotas(int cantidadEquipos, Equipo* equipos);
void ordenGoles(int cantidadEquipos, Equipo* equipos);

int main()
{
    int cantidadEquipos = 0, sigoRecibiendo = 1;
    Equipo equipos[50];
    while (sigoRecibiendo)
    {
        printf("Ingrese información sobre un equipo de fútbol:\n");

        printf("Nombre: ");
        scanf("%s", equipos[cantidadEquipos].nombre);
        printf("Número de victorias: ");
        scanf("%i", &equipos[cantidadEquipos].victorias);
        printf("Número de derrotas: ");
        scanf("%i", &equipos[cantidadEquipos].derrotas);
        printf("Número de empates: ");
        scanf("%i", &equipos[cantidadEquipos].empates);
        printf("Cantidad de goles: ");
        scanf("%i", &equipos[cantidadEquipos].goles);

        printf("¿Desea ingresar información sobre otro equipo?. Sí = 1 / No = 0. \n");
        scanf("%i", &sigoRecibiendo);
        cantidadEquipos++;
        printf("\n");
    }

    int inputUsuario = 0;

    while (inputUsuario != 1 || inputUsuario != 2 || inputUsuario != 3 || inputUsuario != 4)
    {
        printf("Por favor ingrese uno de los siguientes comandos:\n");
        printf("1- Mostrar todos los equipos.\n");
        printf("2- Mostrar todos los equipos ordenadas por victorias.\n");
        printf("3- Mostrar todos los equipos ordenadas por derrotas.\n");
        printf("4- Mostrar todos los equipos ordenados por cantidad de goles.\n");
        int inputUsuario;
        scanf("%i", &inputUsuario);
        printf("\n");

        if (inputUsuario == 1)
        {
            printf("----------- Liga de fútbol -------------\n");
            for (int i = 0; i < cantidadEquipos; i++)
            {
                printf("Nombre: %s\n", (equipos[i].nombre));
                printf("Victorias: %i\n", (equipos[i].victorias));
                printf("Derrotas: %i\n", (equipos[i].derrotas));
                printf("Empates: %i\n", (equipos[i].empates));
                printf("Goles: %i\n", (equipos[i].goles));
                printf("\n\n");
            }

            break;
        }
        else if (inputUsuario == 2)
        {
            printf("----- Equipos ordenados por victorias -----\n");
            ordenVictorias(cantidadEquipos, equipos);
            break;
        }
        else if (inputUsuario == 3)
        {
            printf("----- Equipos ordenados por derrotas -----\n");
            ordenDerrotas(cantidadEquipos, equipos);
            break;
        }
        else if (inputUsuario == 4)
        {
            printf("--- Equipos ordenados por cantidad de goles ---\n");
            ordenGoles(cantidadEquipos, equipos);
            break;
        }
    }
}

void ordenVictorias(int cantidadEquipos, Equipo* equipos)
{
    int i = 0;
    int j = 0;
    Equipo temp;
    
    for (i = 0; i < cantidadEquipos -1; i++)
    {
        if ((equipos[i].victorias) < (equipos[i + 1].victorias))
        {
            temp = equipos[i];
            equipos[i] = equipos[i + 1];
            equipos[i + 1] = temp;
        }
    }

    char guiones[] = "--------------------";
    printf("+%s+%s+\n", guiones, guiones);
    printf("|%-20s|%-20s|\n", "EQUIPO", "VICTORIAS");
    printf("+%s+%s+\n", guiones, guiones);

    for (int i = 0; i < cantidadEquipos; i++)
    {
        printf("|%-20s|%-20d|\n", (equipos[i].nombre), (equipos[i].victorias));
    }
    printf("+%s+%s+\n", guiones, guiones);
    printf("\n\n");
}

void ordenDerrotas(int cantidadEquipos, Equipo* equipos)
{
    int i = 0;
    int j = 0;
    Equipo temp;
    
    for (i = 0; i < cantidadEquipos -1; i++)
    {
        if ((equipos[i].derrotas) < (equipos[i + 1].derrotas))
        {
            temp = equipos[i];
            equipos[i] = equipos[i + 1];
            equipos[i + 1] = temp;
        }
    }

    char guiones[] = "--------------------";
    printf("+%s+%s+\n", guiones, guiones);
    printf("|%-20s|%-20s|\n", "EQUIPO", "DERROTAS");
    printf("+%s+%s+\n", guiones, guiones);

    for (int i = 0; i < cantidadEquipos; i++)
    {
        printf("|%-20s|%-20d|\n", (equipos[i].nombre), (equipos[i].derrotas));
    }
    printf("+%s+%s+\n", guiones, guiones);
    printf("\n\n");
}

void ordenGoles(int cantidadEquipos, Equipo* equipos)
{
    int i = 0;
    int j = 0;
    Equipo temp;
    
    for (i = 0; i < cantidadEquipos -1; i++)
    {
        if ((equipos[i].goles) < (equipos[i + 1].goles))
        {
            temp = equipos[i];
            equipos[i] = equipos[i + 1];
            equipos[i + 1] = temp;
        }
    }

    char guiones[] = "--------------------";
    printf("+%s+%s+\n", guiones, guiones);
    printf("|%-20s|%-20s|\n", "EQUIPO", "GOLES");
    printf("+%s+%s+\n", guiones, guiones);

    for (int i = 0; i < cantidadEquipos; i++)
    {
        printf("|%-20s|%-20d|\n", (equipos[i].nombre), (equipos[i].goles));
    }
    printf("+%s+%s+\n", guiones, guiones);
    printf("\n\n");
}